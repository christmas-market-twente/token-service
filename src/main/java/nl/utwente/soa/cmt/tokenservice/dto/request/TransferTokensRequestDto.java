package nl.utwente.soa.cmt.tokenservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransferTokensRequestDto {

    @NotNull
    @Size(min = 1)
    @Valid
    private List<TransactionRequestDto> transactions;
}
