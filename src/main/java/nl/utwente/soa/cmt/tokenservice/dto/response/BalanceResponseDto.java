package nl.utwente.soa.cmt.tokenservice.dto.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class BalanceResponseDto {

    private Integer balance;

    public BalanceResponseDto(Integer balance) {
        this.balance = balance;
    }

    public BalanceResponseDto() {}
}
