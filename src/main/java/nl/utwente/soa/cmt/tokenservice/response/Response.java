package nl.utwente.soa.cmt.tokenservice.response;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class Response {

    private boolean success;

    public Response() {}

    public Response(boolean success) {
        this.success = success;
    }
}
