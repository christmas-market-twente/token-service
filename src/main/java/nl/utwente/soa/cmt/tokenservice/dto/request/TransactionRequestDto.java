package nl.utwente.soa.cmt.tokenservice.dto.request;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@JsonIgnoreProperties(ignoreUnknown = true)
public class TransactionRequestDto {

    @NotNull
    private String toAccount;

    @NotNull
    @Min(1)
    private Integer amount;
}
