package nl.utwente.soa.cmt.tokenservice.service;

import nl.utwente.soa.cmt.tokenservice.dto.request.BuyTokensRequestDto;
import nl.utwente.soa.cmt.tokenservice.dto.request.TransactionRequestDto;
import nl.utwente.soa.cmt.tokenservice.dto.request.TransferTokensRequestDto;
import nl.utwente.soa.cmt.tokenservice.dto.response.BalanceResponseDto;
import nl.utwente.soa.cmt.tokenservice.exception.InsufficientBalanceException;
import nl.utwente.soa.cmt.tokenservice.model.Transaction;
import nl.utwente.soa.cmt.tokenservice.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class TokenService {

    @Autowired
    private TransactionRepository transactionRepository;

    public BalanceResponseDto getBalance(String account) {
        int balance = transactionRepository.getBalanceOf(account);
        return new BalanceResponseDto(balance);
    }

    public void buyTokens(BuyTokensRequestDto buyTokensRequestDto, String account) {
        // Skip payment checking, including token amount check
        Transaction transaction = new Transaction();
        transaction.setFromAccount(null);
        transaction.setToAccount(account);
        transaction.setAmount(buyTokensRequestDto.getAmount());
        transaction.setTimestamp(new Date().getTime() / 1000);
        transactionRepository.save(transaction);
    }

    public void transferTokens(TransferTokensRequestDto transferTokensRequestDto, String fromAccount) throws InsufficientBalanceException {
        // TODO: make this safe for multithreading
        int balance = transactionRepository.getBalanceOf(fromAccount);
        int totalTransactionAmount = transferTokensRequestDto
            .getTransactions()
            .stream()
            .map(TransactionRequestDto::getAmount)
            .reduce(0, Integer::sum);

        if (balance < totalTransactionAmount) {
            throw new InsufficientBalanceException();
        }

        List<Transaction> transactions = new ArrayList<>();
        for (TransactionRequestDto transactionDto : transferTokensRequestDto.getTransactions()) {
            Transaction transaction = new Transaction();
            transaction.setFromAccount(fromAccount);
            transaction.setToAccount(transactionDto.getToAccount());
            transaction.setAmount(transactionDto.getAmount());
            transaction.setTimestamp(new Date().getTime() / 1000);
            transactions.add(transaction);
        }

        transactionRepository.saveAll(transactions);
    }
}
