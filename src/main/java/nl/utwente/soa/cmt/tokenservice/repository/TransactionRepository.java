package nl.utwente.soa.cmt.tokenservice.repository;

import nl.utwente.soa.cmt.tokenservice.model.Transaction;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

public interface TransactionRepository extends CrudRepository<Transaction, Long> {

    @Query(
        value = "SELECT (SELECT COALESCE(SUM(amount), 0) FROM transactions WHERE to_account = :account) - (SELECT COALESCE(SUM(amount), 0) FROM transactions WHERE from_account = :account)",
        nativeQuery = true
    )
    int getBalanceOf(@Param("account") String account);
}
