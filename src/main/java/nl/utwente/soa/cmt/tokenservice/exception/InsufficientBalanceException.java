package nl.utwente.soa.cmt.tokenservice.exception;

import org.springframework.http.HttpStatus;

public class InsufficientBalanceException extends ResponseException {

    public InsufficientBalanceException() {
        super(HttpStatus.CONFLICT, "Insufficient balance");
    }
}
