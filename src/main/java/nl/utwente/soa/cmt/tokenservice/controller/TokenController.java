package nl.utwente.soa.cmt.tokenservice.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import nl.utwente.soa.cmt.tokenservice.dto.request.BuyTokensRequestDto;
import nl.utwente.soa.cmt.tokenservice.dto.request.TransferTokensRequestDto;
import nl.utwente.soa.cmt.tokenservice.dto.response.BalanceResponseDto;
import nl.utwente.soa.cmt.tokenservice.exception.InsufficientBalanceException;
import nl.utwente.soa.cmt.tokenservice.response.ErrorResponse;
import nl.utwente.soa.cmt.tokenservice.service.TokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/tokens")
@Tag(name = "Token", description = "The Token API")
public class TokenController {

    @Autowired
    private TokenService tokenService;

    @Operation(summary = "Get balance.", description = "Returns the balance of a user.", tags = {"Token"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation")})
    @GetMapping("/balance")
    public BalanceResponseDto getBalance(@Parameter(hidden = true) @RequestHeader("X-User-UUID") String userUUID) {
        return tokenService.getBalance(userUUID);
    }

    @Operation(summary = "Buy tokens.", description = "Adds the specified balance to the account of a user and returns the new balance.", tags = {"Token"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/buy")
    public BalanceResponseDto buyTokens(@Parameter(description = "Payment intent of user and the amount of tokens to be added.")
                                            @Valid @RequestBody BuyTokensRequestDto buyTokensRequestDto,
                                        @Parameter(hidden = true) @RequestHeader("X-User-UUID") String userUUID) {
        tokenService.buyTokens(buyTokensRequestDto, userUUID);
        return tokenService.getBalance(userUUID);
    }

    @Operation(summary = "Transfer tokens.", description = "Transfers the specified amount of tokens from one account to the other.", tags = {"Token"})
    @ApiResponses(value = {
        @ApiResponse(responseCode = "200", description = "Successful operation"),
        @ApiResponse(responseCode = "400", description = "Invalid request body",
            content = @Content(schema = @Schema(implementation = ErrorResponse.class)))})
    @PostMapping("/transfer")
    public BalanceResponseDto transferTokens(@Parameter(description = "The amount of tokens to be transferred and the corresponding accounts.")
                                                @Valid @RequestBody TransferTokensRequestDto transferTokensRequestDto,
                                             @Parameter(hidden = true) @RequestHeader("X-User-UUID") String userUUID) throws InsufficientBalanceException {
        tokenService.transferTokens(transferTokensRequestDto, userUUID);
        return tokenService.getBalance(userUUID);
    }
}
