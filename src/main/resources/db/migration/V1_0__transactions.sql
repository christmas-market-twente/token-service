CREATE TABLE `transactions` (
    `id` BIGINT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `from_account` VARCHAR(36),
    `to_account` VARCHAR(36) NOT NULL,
    `amount` INT UNSIGNED NOT NULL,
    `timestamp` BIGINT UNSIGNED NOT NULL,
    INDEX (`from_account`),
    INDEX (`to_account`)
);